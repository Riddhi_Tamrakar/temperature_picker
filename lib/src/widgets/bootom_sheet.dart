import 'package:temperature_picker_app/src/services/utility.dart';
import 'package:temperature_picker_app/src/styles/theme.dart';
import 'package:temperature_picker_app/src/widgets/common_button_widget.dart';
import 'package:temperature_picker_app/src/widgets/shadow_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomSheetSwitch extends StatefulWidget {
  BottomSheetSwitch({@required this.valueChanged, @required this.tempType});

  final ValueChanged valueChanged;
  final ValueChanged tempType;

  @override
  _BottomSheetSwitch createState() => _BottomSheetSwitch();
}

class _BottomSheetSwitch extends State<BottomSheetSwitch> {
  int _value = 1;
  RangeValues _currentRangeValues = const RangeValues(4, 48);
  RangeValues _rangeValues = RangeValues(4, 48);

  var _minValue;
  var _maxValue;

  @override
  void initState() {
    super.initState();
    convertFrenTocelcius(_currentRangeValues);
  }

  Widget setTemp() => Container(
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context) * 0.25,
          buttonText: 'Set',
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: Colors.green,
          borderColor: Colors.green,
          textColor: AppTheme.kFontColor2,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.020,
          onTap: (val) {
            setState(() {
              _rangeValues = RangeValues(_minValue, _maxValue);
              widget.valueChanged(_rangeValues);
              widget.tempType(_value);
            });
            Navigator.pop(context);
          },
        ),
      );
  Widget currentTemprature(String temp, String dataType) => CustomCard(
        color: AppTheme.kPrimaryColor.withOpacity(0.2),
        child: Container(
          padding: EdgeInsets.all(Utility.displayWidth(context) * 0.06),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _value == 1
                  ? Text(
                      "$temp  \u2103",
                      style: Theme.of(context).textTheme.headline6,
                    )
                  : _value == 2
                      ? Text(
                          "$temp  \u2109",
                          style: Theme.of(context).textTheme.headline6,
                        )
                      : Text(
                          "$temp  K",
                          style: Theme.of(context).textTheme.headline6,
                        ),
              Text(
                dataType,
                style: Theme.of(context).textTheme.headline6,
              ),
            ],
          ),
        ),
      );

  convertFrenTocelcius(value) {
    setState(() {
      _minValue = ((value.start - 30) / 2);
      _maxValue = ((value.end - 30) / 2);
    });
  }

  converCelciusToFereh(value) {
    setState(() {
      _minValue = (2 * value.start + 30);
      _maxValue = (2 * value.end + 30);
    });
  }

  converToKelvin(value) {
    setState(() {
      _minValue = value.start + 273;
      _maxValue = value.end + 273;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(Utility.displayWidth(context) * 0.06),
        color: AppTheme.kPrimaryColor,
        child: Container(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Pick Temperature",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  setTemp(),
                ],
              ),
              SizedBox(
                height: Utility.displayHeight(context) * 0.04,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Radio(
                    activeColor: Colors.red,
                    value: 1,
                    groupValue: _value,
                    onChanged: (value) {
                      setState(() {
                        _value = 1;
                      });
                      convertFrenTocelcius(_currentRangeValues);
                    },
                  ),
                  Text(
                    "\u2103",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Radio(
                    activeColor: Colors.red,
                    value: 2,
                    groupValue: _value,
                    onChanged: (value) {
                      setState(() {
                        _value = 2;
                      });
                      converCelciusToFereh(_currentRangeValues);
                    },
                  ),
                  Text(
                    "\u2109",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Radio(
                    activeColor: Colors.red,
                    value: 3,
                    groupValue: _value,
                    onChanged: (value) {
                      setState(() {
                        _value = 3;
                      });
                      converToKelvin(_currentRangeValues);
                    },
                  ),
                  Text(
                    "K",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
              SizedBox(
                height: Utility.displayHeight(context) * 0.04,
              ),
              RangeSlider(
                values: _currentRangeValues,
                min: 0,
                max: 50,
                divisions: 20,
                activeColor: Colors.red,
                inactiveColor: Colors.white,
                labels: RangeLabels(
                  _currentRangeValues.start.round().toString(),
                  _currentRangeValues.end.round().toString(),
                ),
                onChanged: (RangeValues values) {
                  setState(() {
                    _currentRangeValues = values;
                  });
                  if (_value == 2) {
                    converCelciusToFereh(_currentRangeValues);
                  } else if (_value == 1) {
                    convertFrenTocelcius(_currentRangeValues);
                  } else {
                    converToKelvin(_currentRangeValues);
                  }
                },
              ),
              SizedBox(
                height: Utility.displayHeight(context) * 0.04,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  currentTemprature("$_minValue", "min"),
                  currentTemprature("$_maxValue", "max"),
                ],
              )
            ],
          ),
        ));
  }
}
