import 'package:temperature_picker_app/src/services/utility.dart';
import 'package:temperature_picker_app/src/styles/theme.dart';
import 'package:temperature_picker_app/src/widgets/common_button_widget.dart';
import 'package:temperature_picker_app/src/widgets/shadow_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class BottomSheetInvitePeople extends StatefulWidget {
  BottomSheetInvitePeople(
      {@required this.valueChanged,
      @required this.externalEmailChanged,
      @required this.addExternalUser});

  final ValueChanged valueChanged;
  final ValueChanged externalEmailChanged;
  final bool addExternalUser;

  @override
  _BottomSheetSwitch createState() => _BottomSheetSwitch();
}

class _BottomSheetSwitch extends State<BottomSheetInvitePeople> {
  List userList = ["Abhishek@gmail.com", "Abhay@gmail.com", "Reena@gmail.com"];
  List tempUserList = List();
  List externalUserUserList = List();
  final _emailController = TextEditingController();
  String username;

  @override
  void initState() {
    super.initState();
    print(externalUserUserList);
  }

  void showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.white,
        textColor: Colors.red);
  }

  Widget addUser(obj, index) => Container(
        padding: EdgeInsets.all(Utility.displayWidth(context) * 0.02),
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context) * 0.17,
          buttonText: tempUserList.contains(obj[index]) ? 'Added' : "Add",
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: tempUserList.contains(obj[index])
              ? AppTheme.kPrimaryColor
              : AppTheme.kFontColor2,
          borderColor: tempUserList.contains(obj[index])
              ? AppTheme.kFontColor2
              : AppTheme.kFontColor2,
          textColor: Colors.green,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.015,
          onTap: (val) {
            if (tempUserList.length < 3) {
              if (tempUserList.contains(obj[index])) {
              } else {
                tempUserList.add(obj[index]);
                setState(() {});
              }
            } else {
              showToast("All People Selected");
            }
          },
        ),
      );

  Widget update() => Container(
        padding: EdgeInsets.all(Utility.displayWidth(context) * 0.02),
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context),
          buttonText: 'Done',
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: AppTheme.kFontColor2,
          borderColor: AppTheme.kFontColor2,
          textColor: Colors.green,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.024,
          onTap: (val) {
            widget.valueChanged(tempUserList);
            Navigator.pop(context);
          },
        ),
      );

  Widget _addEmailButton() => Container(
        padding: EdgeInsets.all(Utility.displayWidth(context) * 0.02),
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context),
          buttonText: 'Add Email',
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: AppTheme.kFontColor2,
          borderColor: AppTheme.kFontColor2,
          textColor: Colors.green,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.024,
          onTap: (val) {
            if (_emailController.text != null && _emailController.text != "") {
              if (!RegExp(
                      r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                  .hasMatch(_emailController.text)) {
                return showToast('Please enter a valid email Address');
              } else {
                externalUserUserList.add(_emailController.text);
                print(externalUserUserList);
                _emailController.clear();
                setState(() {});
              }
            } else {
              showToast("Enter Email First.");
            }
          },
        ),
      );
  Widget _doneButton() => Container(
        padding: EdgeInsets.all(Utility.displayWidth(context) * 0.02),
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context) * 0.2,
          buttonText: 'Done',
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: AppTheme.kFontColor2,
          borderColor: AppTheme.kFontColor2,
          textColor: Colors.green,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.020,
          onTap: (val) {
            if (externalUserUserList.length != 0) {
              widget.externalEmailChanged(externalUserUserList);
              print(externalUserUserList);
              Navigator.pop(context);
            } else {
              showToast("Add Emails First");
            }
          },
        ),
      );

  Widget emailList(user) => Container(
        decoration: BoxDecoration(
          color: AppTheme.kPrimaryColor.withOpacity(0.4),
          border: Border.all(color: AppTheme.kPrimaryColor.withOpacity(0.2)),
          borderRadius:
              BorderRadius.circular(Utility.displayWidth(context) * 0.08),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 0,
              blurRadius: 1,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: Utility.displayWidth(context) * 0.02,
              ),
              Text(
                "$user",
                style: Theme.of(context).textTheme.headline3.copyWith(
                      fontSize: AppTheme.kBodyText1FontSize,
                    ),
              ),
              IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    externalUserUserList.remove(user);
                    setState(() {});
                    print(externalUserUserList);
                  })
            ],
          ),
        ),
      );

  Widget addExternalEmail() => Container(
        child: Column(
          children: [
            SizedBox(
              height: Utility.displayHeight(context) * 0.02,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "ADD External Emails",
                  style: Theme.of(context).textTheme.headline6,
                ),
                _doneButton()
              ],
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.02,
            ),

            TextFormField(
              // onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
              keyboardType: TextInputType.emailAddress,
              onTap: () {
                print("hello");
              },
              controller: _emailController,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Email is required';
                }

                if (!RegExp(
                        r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                    .hasMatch(value)) {
                  return 'Please enter a valid email Address';
                }

                return null;
              },
              onSaved: (value) => username = value,
              style: Theme.of(context).textTheme.caption,
              decoration: InputDecoration(
                labelText: 'EMAIL',
                errorStyle: TextStyle(
                    fontSize: 10, color: Theme.of(context).primaryColor),
              ),
            ),
            // ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.04,
            ),
            externalUserUserList != null && externalUserUserList.length != 0
                ? Container(
                    child: Wrap(
                        direction: Axis.horizontal,
                        children: externalUserUserList.map((e) {
                          return Container(
                            child: Padding(
                              padding: EdgeInsets.all(
                                  Utility.displayWidth(context) * 0.010),
                              child: emailList(e),
                            ),
                          );
                        }).toList()),
                  )
                : Container(
                    child: Text("No External Email Added",
                        style: Theme.of(context).textTheme.headline3),
                  ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.04,
            ),
            _addEmailButton()
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(Utility.displayWidth(context) * 0.06),
        color: AppTheme.kPrimaryColor,
        child: Container(
          child: widget.addExternalUser
              ? Column(
                  children: [addExternalEmail()],
                )
              : Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Invite People",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Utility.displayHeight(context) * 0.04,
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: userList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                                trailing: addUser(userList, index),
                                title: Text(
                                  "${userList[index]}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline3
                                      .copyWith(
                                          fontSize:
                                              AppTheme.kBodyText2FontSize),
                                ));
                          }),
                    ),
                    update()
                  ],
                ),
        ));
  }
}
