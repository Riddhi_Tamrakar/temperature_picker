import 'package:flutter/material.dart';
import 'package:temperature_picker_app/src/services/utility.dart';

class AppTheme {
  //
  AppTheme._();

  //Colors

  static const kPrimaryColor = Color(0xfff2c3e4c);
  static const kBackgroundColor = Color(0xffF8F8F8);
  static const kOnPrimaryColor = Colors.white;
  static const kSecondaryColor = Color(0xffa6a6a6);
  static const kIconColor = Color(0xffa6a6a6);
  static const Color kFontColor1 = Colors.black;
  static const Color kFontColor2 = Colors.white;
  static const Color kOutlineColor = Color(0xffb7b7b7);

  //Font-sizes
  static const double kButtonFontSize = 18.0;
  static const double kCaptionFontSize = 13.0;
  static const double kPageTitleFontSize = 27.0;
  static const double kPageTitleFontSize2 = 24.0;
  static const double kTitleFontSize = 22.0;
  static const double kItemTitleFontSize = 20.0;
  static const double kBodyText1FontSize = 12.0;
  static const double kBodyText2FontSize = 14.0;
  //Borders
  static const double kBorderRadius = 10.0;
  static const double kBorderWidth = 1.0;

  //Paddings
  static const double kBodyPadding = 20.0;

  //Others

  static final ThemeData lightTheme = ThemeData(
    fontFamily: 'Poppins',
    primaryColor: kPrimaryColor,
    errorColor: Colors.red,
    scaffoldBackgroundColor: kBackgroundColor,
    appBarTheme: AppBarTheme(
      foregroundColor: kOnPrimaryColor,
      backgroundColor: kPrimaryColor,
      iconTheme: IconThemeData(
        color: kPrimaryColor,
      ),
    ),
    colorScheme: ColorScheme.light(
      primary: kPrimaryColor,
      onPrimary: kOnPrimaryColor,
      // primaryVariant: Colors.white38,
      secondary: kSecondaryColor,
    ),
    cardTheme: CardTheme(
      color: Colors.teal,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: kPrimaryColor, foregroundColor: kOnPrimaryColor),
    iconTheme: IconThemeData(
      color: kIconColor,
    ),
    textTheme: TextTheme(
      headline6: TextStyle(
          fontFamily: 'Poppins',
          // Being used as page title
          color: kFontColor2,
          fontSize: kPageTitleFontSize,
          fontWeight: FontWeight.bold),
      headline3: TextStyle(
          fontFamily: 'Poppins',
          color: kFontColor2.withOpacity(0.6),
          fontSize: kButtonFontSize,
          fontWeight: FontWeight.bold),
    ),
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: TextStyle(
          fontSize: kCaptionFontSize,
          color: kFontColor2,
          fontWeight: FontWeight.w500),
      labelStyle: TextStyle(
          fontSize: kCaptionFontSize,
          color: kFontColor2,
          fontWeight: FontWeight.w500),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(width: kBorderWidth, color: kOnPrimaryColor),
          borderRadius: BorderRadius.circular(kBorderRadius)),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(width: kBorderWidth, color: kOnPrimaryColor),
          borderRadius: BorderRadius.circular(kBorderRadius)),
    ),
  );
}
