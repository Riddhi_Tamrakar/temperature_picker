import 'package:temperature_picker_app/src/services/utility.dart';
import 'package:temperature_picker_app/src/styles/theme.dart';
import 'package:temperature_picker_app/src/widgets/common_button_widget.dart';
import 'package:temperature_picker_app/src/widgets/invite_people.dart';
import 'package:flutter/material.dart';

class EmailPickerPage extends StatefulWidget {
  @override
  _EmailPickerPageState createState() => _EmailPickerPageState();
}

class _EmailPickerPageState extends State<EmailPickerPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List userList = ["Abhishek@gmail.com", "Abhay@gmail.com", "Abhay@gmail.com"];
  List tempUser = List();
  List externalUserEmail = List();

  void initState() {
    super.initState();
  }

  Widget emailList(user, userList) => Container(
        decoration: BoxDecoration(
          color: AppTheme.kPrimaryColor.withOpacity(0.4),
          border: Border.all(color: AppTheme.kPrimaryColor.withOpacity(0.2)),
          borderRadius:
              BorderRadius.circular(Utility.displayWidth(context) * 0.08),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 0,
              blurRadius: 1,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: Utility.displayWidth(context) * 0.02,
              ),
              Text(
                "$user",
                style: Theme.of(context).textTheme.headline3.copyWith(
                      fontSize: AppTheme.kBodyText1FontSize,
                    ),
              ),
              IconButton(
                  icon: Icon(
                    Icons.close,
                  ),
                  onPressed: () {
                    userList.remove(user);
                    setState(() {});
                    print(tempUser);
                  })
            ],
          ),
        ),
      );
  Widget updateButton() => Container(
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context) * 0.85,
          buttonText: 'Update',
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: Colors.red,
          borderColor: Colors.red,
          textColor: AppTheme.kFontColor2,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.030,
          onTap: (val) {},
        ),
      );
  Widget peopleList() => Container(
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "INVITE PEOPLE",
                  style: Theme.of(context).textTheme.headline3,
                ),
                IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    onPressed: () {
                      showModalBottomSheet<void>(
                          context: context,
                          builder: (context) {
                            return BottomSheetInvitePeople(
                              addExternalUser: false,
                              valueChanged: (value) {
                                setState(() {
                                  tempUser = value;
                                });
                              },
                              externalEmailChanged: (value) {},
                            );
                          });
                    })
              ],
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.02,
            ),
            tempUser != null && tempUser.length != 0
                ? Container(
                    child: Wrap(
                        direction: Axis.horizontal,
                        children: tempUser.map((e) {
                          return Padding(
                            padding: EdgeInsets.all(
                                Utility.displayWidth(context) * 0.010),
                            child: emailList(e, tempUser),
                          );
                        }).toList()),
                  )
                : Container(
                    padding: EdgeInsets.only(
                        bottom: Utility.displayWidth(context) * 0.04,
                        top: Utility.displayWidth(context) * 0.04),
                    child: Text(
                      "No People Added",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontSize: AppTheme.kBodyText2FontSize),
                    ),
                  ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.02,
            ),
            Row(
              children: [
                Text(
                  "ADD External Emails",
                  style: Theme.of(context).textTheme.headline3,
                ),
                IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    onPressed: () {
                      showModalBottomSheet<void>(
                          isScrollControlled: true,
                          context: context,
                          builder: (context) {
                            return BottomSheetInvitePeople(
                              addExternalUser: true,
                              valueChanged: (value) {
                                setState(() {
                                  tempUser = value;
                                });
                              },
                              externalEmailChanged: (value) {
                                setState(() {
                                  externalUserEmail = value;
                                });
                              },
                            );
                          });
                    })
              ],
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.02,
            ),
            externalUserEmail != null && externalUserEmail.length != 0
                ? Wrap(
                    direction: Axis.horizontal,
                    children: externalUserEmail.map((e) {
                      return Padding(
                        padding: EdgeInsets.all(
                            Utility.displayWidth(context) * 0.010),
                        child: emailList(e, externalUserEmail),
                      );
                    }).toList())
                : Container(
                    padding: EdgeInsets.only(
                        bottom: Utility.displayWidth(context) * 0.04,
                        top: Utility.displayWidth(context) * 0.04),
                    child: Text(
                      "No External Email Added",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontSize: AppTheme.kBodyText2FontSize),
                    ),
                  ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.1,
            ),
            updateButton(),
            SizedBox(
              height: Utility.displayHeight(context) * 0.02,
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 3.0,
          backgroundColor: AppTheme.kPrimaryColor,
          title: Text(
            "Add People",
            style: Theme.of(context).textTheme.headline6,
          ),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              color: AppTheme.kOnPrimaryColor,
            ),
          ),
        ),
        body: Container(
          color: AppTheme.kPrimaryColor,
          child: Padding(
            padding: EdgeInsets.all(Utility.displayHeight(context) * 0.02),
            child: ListView(
              children: [
                SizedBox(
                  height: Utility.displayHeight(context) * 0.02,
                ),
                peopleList()
              ],
            ),
          ),
        ));
  }
}
