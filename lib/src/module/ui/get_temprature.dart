import 'package:temperature_picker_app/src/services/utility.dart';
import 'package:temperature_picker_app/src/styles/theme.dart';
import 'package:temperature_picker_app/src/widgets/bootom_sheet.dart';
import 'package:temperature_picker_app/src/widgets/common_button_widget.dart';
import 'package:temperature_picker_app/src/widgets/shadow_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'email_picker.dart';

class GetTemprature extends StatefulWidget {
  @override
  _GetTempratureState createState() => _GetTempratureState();
}

class _GetTempratureState extends State<GetTemprature> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var _min;
  var _max;
  int _value = 1;

  void initState() {
    super.initState();
  }

  Widget currentTemprature(String temp, String dataType) => CustomCard(
        color: AppTheme.kPrimaryColor.withOpacity(0.2),
        child: Container(
          padding: EdgeInsets.all(Utility.displayWidth(context) * 0.06),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _value == 1
                  ? Text(
                      "$temp  \u2103",
                      style: Theme.of(context).textTheme.headline6,
                    )
                  : _value == 2
                      ? Text(
                          "$temp  \u2109",
                          style: Theme.of(context).textTheme.headline6,
                        )
                      : Text(
                          "$temp  K",
                          style: Theme.of(context).textTheme.headline6,
                        ),
              Text(
                dataType,
                style: Theme.of(context).textTheme.headline6,
              ),
            ],
          ),
        ),
      );
  void showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.white,
        textColor: Colors.red);
  }

  Widget getDetails() => Container(
        child: CommonButtonWidget(
          buttonwidth: Utility.displayWidth(context) * 0.85,
          buttonText: 'Update',
          borderRadius: Utility.displayWidth(context) * 0.02,
          backgroundColor: Colors.red,
          borderColor: Colors.red,
          textColor: AppTheme.kFontColor2,
          isLoading: false,
          fontsize: Utility.displayHeight(context) * 0.030,
          onTap: (val) {
            showModalBottomSheet<void>(
                context: context,
                builder: (context) {
                  return BottomSheetSwitch(
                    valueChanged: (value) {
                      setState(() {
                        _min = value.start;
                        _max = value.end;
                      });
                    },
                    tempType: (value) {
                      setState(() {
                        _value = value;
                      });
                    },
                  );
                });
          },
        ),
      );
  Widget tempratureDetails() => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "ALERT TEMPRATURE THRESHOLD",
                  style: Theme.of(context).textTheme.headline3,
                ),
                Icon(Icons.edit)
              ],
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.05,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                currentTemprature("${_min != null ? _min : 23}", "min"),
                currentTemprature("${_max != null ? _max : 104}", "max")
              ],
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.05,
            ),
            Text(
              "DESCRIPTION",
              style: Theme.of(context).textTheme.headline3,
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.05,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: AppTheme.kFontColor2),
                borderRadius: BorderRadius.circular(AppTheme.kBorderRadius),
              ),
              child: Container(
                padding: EdgeInsets.only(
                    left: Utility.displayWidth(context) * 0.04,
                    bottom: Utility.displayWidth(context) * 0.04,
                    top: Utility.displayWidth(context) * 0.04,
                    right: Utility.displayWidth(context) * 0.5),
                child: Text(
                  "4 To 48 Temp",
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
            SizedBox(
              height: Utility.displayHeight(context) * 0.05,
            ),
            getDetails()
          ],
        ),
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: AppTheme.kPrimaryColor,
        child: Padding(
          padding: EdgeInsets.all(Utility.displayHeight(context) * 0.02),
          child: Column(
            children: [
              SizedBox(
                height: Utility.displayHeight(context) * 0.04,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => EmailPickerPage(
                          // currentIndex: 3,
                          ),
                    ),
                  );
                },
                child: Container(
                  alignment: Alignment.topRight,
                  child: Text(
                    "Edit Details",
                    style: Theme.of(context)
                        .textTheme
                        .headline3
                        .copyWith(color: Colors.red),
                  ),
                ),
              ),
              SizedBox(
                height: Utility.displayHeight(context) * 0.06,
              ),
              tempratureDetails()
            ],
          ),
        ),
      ),
    );
  }
}
